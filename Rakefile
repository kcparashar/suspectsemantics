#!/usr/bin/ruby

posts_directory = "_posts"
dropbox_directory = "~/Dropbox/Blog"

desc "Alias for 'rake list'"
task :default => [:list]

desc "Serve the site on localhost:4000"
task :serve do
    jekyll_process = Process.spawn("jekyll serve -w")
    sass_process   = Process.spawn("sass --watch assets/css/_sass:assets/css --style compressed")
    processes = [jekyll_process, sass_process]
    trap("INT") do
        processes.each do |process|
            Process.kill(9, process) rescue Errno::ESRCH
        end
        exit 0
    end
    processes.each do |process|
        Process.wait(process)
    end
end

desc "Build the site in the _build directory"
task :build do
    system "jekyll build"
end

desc "Get repo status"
task :status do
    system "git status"
end

desc "Get repo history"
task :history do
    system "git log --pretty=oneline --decorate --graph"
end

desc "Get latest updates from the repo"
task :update do
    system "git pull"
    system "rm #{posts_directory}/*"
    system "cp #{dropbox_directory}/* #{posts_directory}"
end

desc "Deploy the site to GitHub Pages"
task :deploy do
    system "git add ."
    system "git commit -m 'Blog updated'"
    system "git push"
end

desc "List all rake tasks"
task :list do
	system "rake -T"
end
