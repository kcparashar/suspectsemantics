---
layout: post
title: Selective Thinning
tags: Thoughts
---

I’ve been tinkering with the design of my site since I updated to the new look. One of the things I wanted to add was the ability to differentiate posts visually when looking through the site, to make some posts “featured” or “important”. For those of you who use Tumblr, you may already be familiar with the general premise, with the way that Tumblr allows for “highlighted” posts. Well, this is similar (although it would be kind of ridiculous for me to charge myself to highlight the posts on my blog).

As I worked on ideas for how to visually differentiate posts, I came up with something. I could just reverse the color. Currently, each post is dark text on a white background, well, how about white text on a dark background? So, I set about doing just that, but as any of you who have worked in web development are familiar, computer font rendering tends to make fonts look fatter when they are on a dark background. So, I had three options: use a lighter background, choose a different font, or figure out a code trick to fix it.

I opted for the last one, and soon realized that I could use something I already knew to get the result I wanted. `-webkit-font-smoothing: antialiased`, which has been used by some to improve the rendering of fonts on their site, has the side effect of making fonts appear thinner. In this case, that is exactly what I wanted. So I dropped in that little CSS property, and suddenly achieved the result I wanted. It’s live now, and you can check out the recent birthday post to see it in action.

It’s not a huge discovery, just a small one, but I hope that someone else can find some utility in it. I know I have.

P.S. Thanks [Tim Van Damme](http://maxvoltar.com "Max Voltar, the Site of Time Van Damme") for originally [bringing my attention](http://maxvoltar.com/archive/-webkit-font-smoothing "Webkit Font Smoothing") to this CSS property so many months ago.
