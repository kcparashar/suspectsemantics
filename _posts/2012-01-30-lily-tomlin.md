---
layout: post
title: Lily Tomlin
tags: Thoughts
---

Last night I saw Lily Tomlin perform. She entered the stage smiling, over-bowing to the crowd as they cheered and clapped for her arrival. She let loose a few jokes about her home town, Detroit, and the fact that, however hard we try, her town is still poorer than ours. They are in first place, we are in second. It was exactly this sort of comedy that came to characterize the entirety of the show. Between moments of political commentary, and revivals of characters that may honestly be past their prime, Tomlin released exacting barbs of competition and success. Her town is first, but in this case, that’s bad. She is a star, but as she pointed out several times, she’s playing for a crowd that is sadly passing away.

The show was therefore a morbid mixture of zest, energy, and yet the macabre understanding that for everyone, accomplishments are fleeting. Comedy is hardly ever timeless, and in a world increasingly connected, the passage of time matters even more than ever.

I’ve always liked Tomlin. While certainly not in her target demographic, I found her dry wit on The West Wing to be something magical, and her chemistry with Martin Sheen to be a periodic highlight of later seasons. She was short with words, wily, and always left those around her with a feeling of befuddlement and begrudging acceptance. She had a childhood friend in the audience that night, and she had brought a yearbook picture with the both of them, which she put on the screen behind her. This was her life, and this was her before the stardom and before Laugh In and before going postal on an unsuspecting boss (and before postal workers truly began to follow suit). She seemed happier then. I can only begin to imagine what has changed. It is before my time, and my time is after hers. And for all the stardom, and all the experiences, and everything that has happened, in public and in secret, at the end of the night all she wanted to do was talk with an old friend, and tell stories about the old days.

Many things have been written about how fame isn’t what it’s cracked up to be. Arthur Conan Doyle killed off Sherlock Holmes because it was overshadowing his other works. Drake spent an entire album rapping about the joy and horrifying loneliness of super-stardom. But as with many things, these things matter little, until you see what they mean for yourself. Last night, I saw a woman, old and full of energy, connect with an audience that just like her is rapidly approaching a time where days will be spent as fertilizer at some grave somewhere. She was bright, she was befuddling, and she was all the same, sad. And tired.

You are amazing Lily Tomlin. I’m glad I got to see you.
