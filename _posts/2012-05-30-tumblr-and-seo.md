---
layout: post
title: Tumblr and SEO
tags: Thoughts
---

Well, as I’ve been fiddling around with Google’s Webmaster Tools for this site, I discovered something pretty cool. It seems that Tumblr automatically generates a robots.txt file for your site, to tell any search engine bots what they can and can not crawl. Not only that, but they also generate a sitemap for you, with every post you’ve ever posted, as well as another sitemap for any pages that you make using the “pages” section of the “Customize Theme” page for your blog. Basically, if you click “show a link to this page” on the edit window for your custom page, Tumblr with generate a sitemap and include a link to it, thereby making it possible for bots like GoogleBot to find it and index it.

So, if you are interested in improving your SEO for a Tumblr blog, it is in your best interest to click “show a link to this page” for any page you’d like Google (or any other search engine) to index.
