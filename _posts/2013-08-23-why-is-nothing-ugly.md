---
layout: post
title: Why Is Nothing Ugly?
excerpt: "<p>The web is not for everyone. Sorry to break it to you, but it’s not. There are, in broad terms, two groups of people the mainstream web is currently for: the perfectly presentable, and the presentably imperfect. The first is the stuff of Medium, TED Talks, and Sunday editorials. People who work at accelerators and can often be found textually bloviating about the “next big thing” from a one-off coffee shop selling complex espressos and accepting payments with Square. The second is the stuff of Vine, viral videos, and activist blogs. People who’ve parlayed “being odd” into some modicum of success or notoriety.</p>"
tags: Thoughts
---

The web is not for everyone.

Sorry to break it to you, but it’s not.

There are, in broad terms, two groups of people the mainstream web is currently for: the perfectly presentable, and the presentably imperfect. The first is the stuff of Medium, TED Talks, and Sunday editorials. People who work at accelerators and can often be found textually bloviating about the “next big thing” from a one-off coffee shop selling complex espressos and accepting payments with Square. The second is the stuff of Vine, viral videos, and activist blogs. People who’ve parlayed “being odd” into some modicum of success or notoriety.

First: don’t hate either of these groups. They are not bad people. Many of them are kind, hard-working people who are trying to improve the world; whether by cheering you up, getting you mad, or sharing just a little bit of knowledge you wouldn’t otherwise have. So don’t hate them.

But know that their success is costly. Not for them, but for you. The average person’s thoughts, pains, feelings are hidden away on the mainstream web. This is partly due to the nature of the web. Good, easy-to-use platforms for creating meaningful content are few and far between. Tumblr is that for some, but their tagging system and assorted recommended blogs still favor the loud, the boisterous, the Big Two. It’s not for the introvert. It’s not for you.

While much has been written about the way the web gives people a voice, there is little said abut the way the current structure encourages conformity. Recommendation engines provide you with content you’ll like. It gives you the popular stuff. It encourages your biases, reinforces you worldview, inflates your ego. Surfing on Tumblr, or scrolling through Twitter, or catching up on Facebook, you are often unlikely to stumble across anything that really challenges you.

And you need to be challenged. We all do. We should all at some point see the ugliness, the pain, the introversion of humanity. There is a great platform, a wonderful opportunity at hand to truly connect with the vastness of humanity. Yet it only gives us ourselves, reflected back. It only feeds us our usual meals, reheated, resealed, cleaned up nice.

So unless you actively seek the other opinion, unless you go scrolling through link after link in search on the challenge, you won’t find it, and it won’t find you.

It’s my goal now to do just that. To find the things that will force me to question how I view the world. And not for a little while. Not as a publicity stunt or flight of fancy, but as an honest and real attempt to become a better human being.

It’s my hope that in the long run the tools and platforms we have will develop to make this easier. That Tumblr and Google and Facebook and Twitter will not only help you find the things you like, but the things you don’t.

Because we could all do with more of each other, and less of ourselves.
