---
layout: post
title: Ending "Hipster"
tags: Thoughts
---

In a recent discussion with my friend William Elder, I was convinced by him to make an interesting change to my current vocabulary. I was convinced to stop using the word “hipster”.

"Hipster" has become this generation’s generic word for people we don’t like. On its face a signifier for some specific subculture, it is underneath such a broad term as to be used to encompass nearly anyone for whom we do not care. What does "hipster" mean, really? Does it mean someone is pretentious? Does it mean they are judgmental? Does it mean they wear a lot of flannel? Does it mean they have a twirly mustache or love irony? Does it mean they have an "I knew that before it was cool" air about them? What does it mean?

As far as I can tell, in this day and age the word “hipster” means nothing. It is our generation’s “hippy” (are they an anti-war activist? Do they wear a lot of tie-dyed clothing? Do they just smoke a lot of pot?), and it is for this reason that I have decided never to use the word “hipster” as a descriptor again.

If I do not like someone, I’d like to be able to identify why I don’t like them. This is not just for personal satisfaction of self-understanding, it is because being narrowing down why I don’t like someone forces me to carefully evaluate (or at least acknowledge) my own prejudices and how they affect my behavior. So, if while talking to someone I discover that they are pretentious and judgmental, I am not going to simply say “that person is a hipster”, I will say “that person was really pretentious and judgmental”. Such a description not only clarifies the issue for me, but also makes descriptions of these individuals to others much clearer, and removes potential ambiguities arising from differing opinions on the proper definition of “hipster”.

Therefore, if I meet you and do not like you I may say a number of things in determining why. I may say you are pretentious. I may say you lack social graces. I may say you presented yourself in a slovenly manner. But I will never again say that you are a hipster, because seriously, what does that even mean now?
