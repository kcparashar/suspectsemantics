---
layout: post
title: A Simple Site Status Checker
tags: Thoughts
---

In an effort to learn more about Ruby I’ve been working a little Ruby Gem. It’s called “isup”, and is a simple command line tool to see if a site is up or down. It currently follows redirects, handles most errors, and includes a basic verbose mode that prints whenever a redirect is followed.

It’s still a work in progress. The code is all [available on Github](https://github.com/andrewbrinker/isup "isup: simple site status checker") if anyone wants to contribute.
