---
layout: post
title: Emotional Distance
tags: Thoughts
---

I have Asperger’s Syndrome. I’ve known for quite some time. It’s not usually a problem, but recently, I’ve come to think of its impact on my life more and more. I’ve decided to start dating, you see, and while I have had a girlfriend before (yes, one) my abilities in this area are sorely lacking, and I’ve found myself floundering a bit. I know that this issue isn’t unique, and that they aren’t necessarily attributable to my Asperger’s, but there is one thing that I think is: the issue of emotional distance.

In an average social situation, there’s a certain level of emotional distance individuals keep and are tacitly expected to keep from each other. That level is directly proportional to the closeness of the relationship. With your close friends, you can be more emotionally open, whereas with acquaintances and people you have just met conversation is usually kept to polite and emotionally distant and uninvolved small talk.

Romantic relationships, the sort of pair-bonds formed from social constructs like dating, have a much smaller inherent level of emotional distance. In a romantic relationship, you not only share opinions and facts, but feelings, in an unbridled way uncommon to most any other social connection. Feelings, and their passage from one half of the bond to the other, are one of the defining aspects of romantic relationships. The small emotional distance is what truly makes romantic relationships romantic.

For those with Asperger’s Syndrome, like myself, empathizing with others is near impossible. While we may with practice become fairly good at mimicking empathy, the actual experience of feeling someone else’s emotions is completely foreign. In a romantic relationship, where emotional closeness is an implicit requirement, this inability can lead to severe emotional distance issues.

I’ve recently read multiple stories from spouses of individuals with Asperger’s, and they all painted the same story: a relationship where one party desires emotional closeness, and the other simultaneously leeches attention from the other while ignoring their partner’s emotional needs. It’s a sad thing to hear about, and exactly this sort of thing that worries me as I begin to date. My Asperger’s is fairly mild, and after years of work, I am largely functional in social situations. But in this area of personal new ground, I am left to wonder and worry about whether I am even suited for a romantic relationship at all.
