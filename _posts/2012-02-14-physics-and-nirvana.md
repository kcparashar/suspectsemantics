---
layout: post
title: Physics and Nirvana
tags: Thoughts
---
I got a 90% on my Physics midterm. It’s okay. I could have done better.

This seems to be a recurring problem. The comparison of actual accomplishments to a percieved potential accomplishment. All achieved things become lesser based on that ideal, and you end up losing one of the greatest motivators for further effort: a sense of accomplishment. It’s called the “Nirvana Fallacy”. That’s the afterlife, not the band. At any rate, as much as I tell myself that 90% is good, I can’t shake the twinging feeling of failure.

I feel like a failure a lot. As I’m spouting off facts to friends, as I’m taking tests, as I’m writing this, I think to myself “No one cares. You’re nothing but an encyclopedia. Your thoughts are shit”. It’s not a good way to think about yourself. Unfortunately, telling yourself that doesn’t seem to matter.

I’ve been going this blog seriously for about two weeks now, and I can’t help but think that I’m getting worse. Maybe some of these posts are like the sudden burst of warmth before you freeze to death. A last ditch attempt to live on. Maybe they’re a precursor to something. You don’t know.

Not knowing is exactly what brings about the Nirvana Fallacy. You don’t know how things would have gone otherwise, had the situation been perfect. So you fabricate a world where you cured cancer, and wrote the great American novel, and got 100%. That world doesn’t exist, but it seems preferable to the one you live in, and so the comparisons live on. You become increasingly morose, only making it harder to snap up and realize that 90% is pretty good.

I think I might be fine with pretty good.
