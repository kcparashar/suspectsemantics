---
layout: post
title: Album Obsessiveness
tags: Thoughts
---

Music is terrific. Scratch that, pretty much all music is terrific. Super generic music of any sort is generally not terrific, whether it is pop, rock, country, or what have you. But that music is not what I am talking about today. No, I am talking about terrific music. I am talking about the music I listen to all the time and love.

Beyond that, I’m talking about a habit. I listen to music all the time, and when I listen, I tend to become a little album obsessive. What I mean by that is, I find an album. I listen to it for a little while. Then I don’t stop. I start with an interest in the songs, picking out a few that I like. I start learning the lyrics, and I start reciting all of them until they’re memorized. Then I move on to the lower quality songs, and sure enough I learn them too. This keeps going, and in fact the best measure of how much I like an album is how long this continues. Right now, the record holder is Bon Iver’s latest album, “Bon Iver” (very creative title), with a total of 208 listens.

Number 2 is Drake’s “Take Care”, with 207 listens (it’s a dead heat with “Bon Iver”, really). Number 3 is fun.’s “Some Nights”. At any rate, there’s not much more to this post except to ask: do you do this? I’d like to know. I mean, I can’t be the only album obsessive person out there, can I?
