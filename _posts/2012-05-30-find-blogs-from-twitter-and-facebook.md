---
layout: post
title: Find Blogs from Twitter and Facebook
tags: Thoughts
---

Tumblr has just launched a small but interesting new feature designed to connect you with blogs to follow. You connect your Tumblr to Facebook and Twitter, and then Tumblr searches through your friends for blogs they have on Tumblr, and suggests those blogs to you. Altogether it’s a pretty neat system, and having just tried it out, I can say that I did in fact find several new blogs I wanted to follow. However, none of those blogs came from Facebook.

The move has already gathered some pretty heated responses, including [a post by “Blogging via Typewriter”](http://inothernews.tumblr.com/post/24079765833/besides-the-hype-besides-the-technical-fuckups-of "Blogging Via Typewriter") bemoaning it. The general complaint is that people don’t want to be forced to use Facebook, or that they generally aren’t interested in things their friends have to blog.

I think this comes down to the type of networking Facebook and Twitter each foster. From what I’ve seen of Facebook (which is anecdotal, but at least offers some frame of reference), people use it to connect with their friends. Friends they see everyday, or friends they don’t. But in both cases, I don’t imagine people are interested in reading the random blogs and reblogs of these friends. If it’s something really great, they’ll hear about in real life. If it’s a friend they’re no longer connected to, they probably just don’t care.

Twitter, however, is where a lot of people go for news, or to follow people in their field. Twitter is, much more than Facebook, a place for thought provoking discussion, or links to thought provoking discussion (contrary to what the character limit would imply). So, when Tumblr suggests blogs operated by people I follow on Twitter, I am generally interested, certainly more so than I am for suggestions from Facebook friends.

I am curious what sort of results Tumblr is seeing from this feature, and whether their data is consistent with my predictions, but if I’m right, the majority of blogs followed using this new feature will be from Twitter, not Facebook.

Update: Another thought is the idea that sometimes people don’t want their blogs to be found. At least for me, as soon as I saw that feature, I made a few of my blogs private (I use them for development, but usually keep them public to test the notes feature) because they don’t exist for other people to look at, they exist for me to work. There are also some people who use Tumblr as an outlet to vent frustrations from their life. But what if you are an emotional teenager (this is to say, a teenager) and you blog about your parents and teachers and such on your blog. You thought you were safe, that it was a safe place to talk about life without people you know seeing it or judging it. Suddenly, your Mom has seen it, and your Dad, and maybe a teacher if you sent them a friend request and they accepted (which is probably a bad idea, and a whole different ball of wax entirely). Now, that thing you intended to be private is very, very public; and it could cause some serious harm.
