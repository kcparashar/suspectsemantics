---
layout: post
title: The Quiet, Quiet Wind
tags: Prose
---

The children played in pastures, running in circumscribed arches, then breaking off cotangentially. The air rushed through their faces, lifting them to fly. Their father smiled as he watched, thin-armed glasses sitting placidly on his face. His smile, small and mirthful, twitched, rose and fell as his mind stretched and collapsed, waxing contemplative over the day. They looked like their mother. He watched his children’s movement with his eyes, which ever yet wandered up and over the fields, to stare at the sun.

Their laughter broke up in the sky somewhere, and turned to dull aches of chartered atmosphere. The father thought of land, his land, and how it was his. His Tourette’s smile rose again. The sun was breaking on the laughter, and falling only calmly down. Abiding on the faces of the kids, it danced and played as they did in their spheres, which tilted about and never changed. The glow it proffered was peace, offered with white flag for their spirits, and equal in measure to them.

Soon they went inside, as the sun’s glow faded off their faces with their electric souls. Sleep even sooner met their eyes, and the laughter passed away, and dinner was consumed, and laughter for a moment resumed, and they slept.

The father, in the kitchen, twitched with will never to shake, and holding something in, broke a little more inside. The roof cracked, and tiles smacked the grass below as they were shaken off like molted skin. And the father, now decrepit and depressed, wept like the trees outside, for the sun sunken down below the edge, and the quiet, quiet wind, and the sphere that for all its tilting, wouldn’t move.
