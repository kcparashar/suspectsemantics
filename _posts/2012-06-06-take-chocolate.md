---
layout: post
title: Take Control, Take Chocolate
tags: Prose
---

We were asked in my Oral Comm class to write a very small impromptu persuasive speech with a topic we were assigned. I was assigned chocolate, and decided to have fun with it. Here is what I wrote:

In Roald Dahl’s “Charlie and the Chocolate Factory”, a young boy and his grandpa meet an odd and at times terrible man. They travel through places both freeing and bizarre, and along the way discover the value of humility and… something else. I don’t know more than that. You know why? Because, for a movie called “Charlie and the Chocolate Factory”, there was not as much chocolate as I expected, and that’s a shame. I am here today to talk to you all about that miracle sweet, that delectable treat, that venerable bastion of sinful self-indulgence: chocolate.

Discovered in 1100 BC, chocolate is the pinnacle of human accomplishment. Does that seem a bit much? Dark chocolate has been shown to be good for the health, increasing libido, and only somtimes poisoning you with lead. It lowers cholesterol, improves brain function, and keeps you feeling young (all in moderation). That’s a lot of good and only a little lead for you and your body. So next time you’re feeling down, and wondering where your life is going, make a choice, take control, and take chocolate.
