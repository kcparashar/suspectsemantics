---
layout: post
title: Exhaustion
tags: Thoughts
---

I’m feeling it right now. I had a Midterm today, and a Computer Science lab due, and review for my Physics Midterm next Tuesday, and I am tired.

When people are tired, they tend to try less. They’re more short tempered, they’re lazier. I’m all of those things, but I have an additional symptom: when I’m tired, my Asperger’s shows more.

When I’m rested, and energized, and alert I can usually stay on top of things socially. I can converse decently, and read facial expressions well enough to get by. When I’m tired, all the practice I’ve put into developing those skills, and all the effort they require of me, goes out the window.

If you’ve ever caught me when I’m tired, and I’ve said something insulting or insensitive, I’m sorry. I didn’t mean to. I didn’t mean to insult your religious beliefs, or pick a fight with you over gay marriage, or anything else. I feel bad.

I’ve found that this is a common thing amongst people with Asperger’s. Basic social functions are more labor intensive for Aspies than they are for allistic people, and the laziness (or at times simple inability to try) that sets in when we’re exhausted make not being an accidental ass a lot more difficult. It’s not an excuse, and if I do something wrong, please call me on it, tired or not. It’s just a statement of fact.

I’ve been trying to force myself to write a blog post each day. It’s a way of improving my writing skills, it’s cathartic, and today is the first day where I’ve actually had to force myself. It was actually only the thought of an audience reading this that made me start writing. I like the attention sometimes; having readers is one of the greatest things I know.

So, if you’re reading this, and in the future I say something rude, or cruel, or whatever, please tell me. I may honestly not have noticed.
