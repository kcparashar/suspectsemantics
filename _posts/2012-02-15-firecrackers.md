---
layout: post
title: Firecrackers
tags: Prose
---

Firecrackers. Stark, quick footed down the corridors. Up and out of bed and launching, lighting firecrackers.

“Why me?” he thought. Having barely fallen bedside, he pictured now the slightest little flames, and saw them billow and smoke to life. He wondered if this warranted caring, and in answer to himself gave little more than a noncommittal grunt. Perhaps the same grunt they would give at the billows, perhaps the one they’d give at the sight of him. He had a hunch, and tossed it sideways walking down the stairs, that drew their eyes. He hated when they stared, but loved when they were quiet. Their distracted stupor, when it came, was therefore favorable to him, and left him with a smile that only animated further the grotesqueness of his state.

As he landed on FLOOR THIRTEEN, words the signage screamed so readily at its entrants, he peered about for flames or sparks that would flag the very spot these hudlums hid. They knew that he was coming. Every action undertaken was telegraphed by wood-panel-wire to the floor below. His steps, hard and heavy, were not once cryptographic tools. He had therefore consigned himself to hunting, stumbling forth in search of bandits, thieves, and Romeos each and every time they struck.

This time it was firecrackers. Originality they have, but few points did this score with him, achingly ajar in his gait, who now found himself the cat to their mouse. What fairness was there in his position? Their limber legs could take them hither and to the basement. Lower still their arms might take them, shovels burying them in neck high holes of effort. He had no such strength, physical or of will, and so these chases often ended in the spot where they began: with him, the man so pained to live, lying crooked in his bed. He counted sheep and burning children to pass the time, and slept.
