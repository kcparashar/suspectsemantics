---
layout: post
title: Being Original
tags: Thoughts
---

For the longest time, I was concerned with being original. I was so focused on saying something, the perfect thing, the most poignant point possible. I somehow had this belief that the people whose quotes you read just magically produced them, that their minds were so clear, and their wits so sharp, that they took one look, and knew it all, and said it all. So I waited, and contemplated, not the issue, but the question of how this spell was done. I waved my arms in desperation, and left myself with questions, and feelings of such deep, deep inadequacy.

"I’m a failure", "I’m stupid", "What happened to me?". An identity crisis of epic proportions occured somewhere on the bus, third seat on the left, I believe. I can’t recall the day. Hadn’t I always been smart? That’s who I am. I had made the fatal mistake of basing my entire self on accomplishments. For those of you who don’t have a self yet, don’t do it, it’s a really stupid thing to do. The inadequacy I felt, the sense of failure, for something so ridiculous that the expectation that I could even accomplish it was a comedy tour in and of itself, was crushing.

But I kept waiting, “persevere”, I said. So I waited, still comtemplated, nothing much at all.

People don’t pull those thoughts out of midair. The man with all the answers is a myth, and the man with even half didn’t get them all for free. You have a good idea by first realizing that a good idea doesn’t happen in a vacuum. Even world-changing analyses are based on rethinkings of life as you know it. Ignoring the world, and attempting to will genius is absurd.

I’ve since given up on being original. I’ll write what I think, and if someone said it before, and I don’t know it, I’ll say it again, and learn. If someone said it before, and I do know it, I’ll build. And people may or may not comment, and my work may be important, or insignificant. But it is my work, and they are my thoughts, and I didn’t get them from just sitting around and waiting. The best thoughts come when you’ve put a lot of thought in, and made yourself a student. They come when you’ve processed, and learned, and made yourself an expert. They come when, after all of that, you let your mind go free.

I need to do that more.
